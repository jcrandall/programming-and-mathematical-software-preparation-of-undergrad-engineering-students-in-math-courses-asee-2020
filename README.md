# Programming and Mathematical Software Preparation of Undergrad Engineering Students in Math Courses - ASEE 2020

Paper and presentation video for ASEE 2020.
Study carried out in 2019. Collected data from math instructors and students in upper-division math courses that support the engineering curriculum. Results about the use of industry-relevant software and programming in math curriculum are reported. 
